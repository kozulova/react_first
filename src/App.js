import './App.css';
import Chat from './Chat';

function App() {
  return (
    <div className="App">
      <Chat url='https://edikdolynskyi.github.io/react_sources/messages.json'/>
    </div>
  );
}

export default App;
