import React from 'react'
import Message from './Message'
import OwnMessage from './OwnMessage'
import _ from 'lodash'
import moment from 'moment'
import {dateToFromNowDaily} from '../helpers/dataformat'

const MessageList = ({ messages, deleteMessage, likeMessage }) => {

    const groupByDate = () => {
        const dateFormated = messages.map(item => {
            item.day = dateToFromNowDaily(item.createdAt);
            return item;
        })
        const groupedMessages = _(dateFormated).groupBy('day').value();
        return groupedMessages;
    }

    const renderMessage = (messagesToRender) => {
        return messagesToRender.map(message => {
            return (message.user === 'Me' ? <OwnMessage message={message} key={message.id} deleteMessage={deleteMessage}/> :
                <Message message={message} key={message.id} likeMessage={likeMessage}/>)
        })
    }
    const renderDivider = (date) => {
        const grouped = groupByDate();
        console.log('render divider', Object.entries(grouped));

        const items = Object.entries(grouped).map((item, index) => {
            const groupMessages = renderMessage(item[1])
            return (
                <>
                    <div className="messages-divider" key={index}>{item[0]}</div>
                    {groupMessages}
                </>
            )
        })
        return items.map(item => item)
    }

    return (
        <div className="message-list">
            {renderDivider()}
        </div>
    )
}

export default MessageList
