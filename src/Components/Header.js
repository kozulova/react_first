import React from 'react'
import _ from 'lodash'
import moment from 'moment'

const Header = ({messages}) => {
    const countParticipants = () => {
        return _.uniq(_.map(messages, 'user')).length;
    }
    const findLastMessage = () => {
        console.log(_.min(_.map(messages, 'createdAt')));
        return _.max(_.map(messages, 'createdAt'));
    }
    findLastMessage();
    countParticipants();
    return (
        <div className = 'header'>
            <h2 className = 'header-title'>CHATNAME</h2>
            <div><span className='header-users-count'>{countParticipants()}</span> participants</div>
            <div><span className='header-messages-count'>{messages.length}</span> messages</div>
            <div>last message at <span className='header-last-message-date'>{moment(findLastMessage()).format("DD.MM.YY HH:MM")}</span></div>
        </div>
    )
}

export default Header
